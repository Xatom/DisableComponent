﻿using System;
using System.Management.Automation;

namespace DisableComponent
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Get interface names and component IDs using Get-NetAdapterBinding
            Console.WriteLine(Toggle(false, "Ethernet", "mc_tcpip6") ?
                "IPv6 has been disabled" : "IPv6 has NOT been disabled");
        }

        private static bool Toggle(bool enabled, string name, string componentId)
        {
            using (var powerShell = PowerShell.Create())
            {
                try
                {
                    var prefix = enabled ? "Enable" : "Disable";
                    powerShell.AddCommand(prefix + "-NetAdapterBinding");

                    powerShell.AddParameter("Name", name);
                    powerShell.AddParameter("ComponentID", componentId);

                    powerShell.Invoke();

                    return powerShell.Streams.Error.Count == 0;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
